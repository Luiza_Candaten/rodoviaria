package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import connection.ConnectionFactory;
import model.bean.Passageiro;

public class PassageiroDAO {
	public void create(Passageiro f) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("INSERT INTO Passageiro (id_cliente, Nome_Completo, Genero_pass, RG, CPF, Rua, Numero, Bairro, Email, Telefone, cartao_de_credito, CVV) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
			stmt.setInt(1, f.getId_cliente());
			stmt.setString(2, f.getNome_Completo());
			stmt.setString(3, f.getGenero_pass());		
			stmt.setString(4, f.getRG());	
			stmt.setString(5, f.getCPF());
			stmt.setString(6, f.getCVV());		
			stmt.setString(7, f.getRua());	
			stmt.setString(8, f.getNumero());
			stmt.setString(9, f.getBairro());		
			stmt.setString(10, f.getEmail());	
			stmt.setString(11, f.getCartao_de_Credito());
			stmt.setString(12, f.getTelefone());		
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}

	public List<Passageiro> read(){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> passageiros = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM Passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Passageiro f = new Passageiro();
				f.setId_cliente(rs.getInt("id_cliente"));
				f.setNome_Completo(rs.getString("Nome_Completo"));
				f.setRG(rs.getString("RG"));
				f.setCPF(rs.getString("CPF"));
				f.setRua(rs.getString("Rua"));
				f.setNumero(rs.getString("Numero"));
				f.setBairro(rs.getString("Bairro"));
				f.setGenero_pass(rs.getString("Genero_pass"));
				f.setEmail(rs.getString("Email"));
				f.setTelefone(rs.getString("Telefone"));
				f.setCVV(rs.getString("CVV"));
				f.setCartao_de_Credito(rs.getString("cartao_de_credito"));
				passageiros.add(f);
			}
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do BD" + e);
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiros;
	}
		
		public void delete(Passageiro f) {
				
				Connection con = ConnectionFactory.getConnection();
				PreparedStatement stmt = null;
				
				try {
					stmt = con.prepareStatement("DELETE FROM Passageiro WHERE CPF=?");
					stmt.setString(1, f.getCPF());
					stmt.executeUpdate();
					
					JOptionPane.showMessageDialog(null, "Passageiro exclu�do com sucesso!");
				}catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro ao excluir: "+ e);
				}finally {
					ConnectionFactory.closeConnection(con, stmt);
		}
	}

		public Passageiro read(int id) {
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement stmt = null;
			ResultSet rs = null;
			Passageiro f = new Passageiro();
			
			try {
				stmt = con.prepareStatement("SELECT * FROM passageiro WHERE id_cliente=? LIMIT 1;");
				stmt.setInt(1, id);
				rs = stmt.executeQuery();
				if(rs != null && rs.next()) {
					f.setId_cliente(rs.getInt("id_cliente"));
					f.setNome_Completo(rs.getString("Nome_Completo"));
					f.setRG(rs.getString("RG"));
					f.setCPF(rs.getString("CPF"));
					f.setRua(rs.getString("Rua"));
					f.setNumero(rs.getString("Numero"));
					f.setBairro(rs.getString("Bairro"));
					f.setGenero_pass(rs.getString("Genero_pass"));
					f.setEmail(rs.getString("Email"));
					f.setTelefone(rs.getString("Telefone"));
					f.setCVV(rs.getString("CVV"));
					f.setCartao_de_Credito(rs.getString("cartao_de_credito"));
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				ConnectionFactory.closeConnection(con, stmt, rs);
			}
			return f;
		}
		
		public void update(Passageiro f) {
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement stmt = null;
			
			try {
				stmt = con.prepareStatement("UPDATE passageiro SET Nome_Completo=?, Genero_pass=?, RG=?, CPF=?, Rua=?, Numero=?, Bairro=?, EMAIL=?, Telefone=?, cartao_de_credito=?, CVV=?"+ " WHERE id_cliente=?;");
				stmt.setInt(1, f.getId_cliente());
				stmt.setString(2, f.getNome_Completo());
				stmt.setString(3, f.getGenero_pass());		
				stmt.setString(4, f.getRG());	
				stmt.setString(5, f.getCPF());
				stmt.setString(6, f.getCVV());		
				stmt.setString(7, f.getRua());	
				stmt.setString(8, f.getNumero());
				stmt.setString(9, f.getBairro());		
				stmt.setString(10, f.getEmail());	
				stmt.setString(11, f.getCartao_de_Credito());
				stmt.setString(12, f.getTelefone());
				stmt.executeUpdate();
				
				JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
			}catch (SQLException e) {
				JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
			} finally {
				ConnectionFactory.closeConnection(con, stmt);
			}
		}
			
	}