package model.bean;

public class Passageiro {

	private int idPassagem;
	private String Nome_Completo;
	private int numero_de_passagens;
	private String CPF;
	private String RG;
	private String Cartao_de_Credito;
	private String CVV;
	private String Genero_pass;
	private String Email;
	private String senha;
	private String Rua;
	private String Numero;
	private String Bairro;
	private int id_cliente;
	private String Telefone;
	public int getIdPassagem() {
		
		return idPassagem;
	}
	public void setIdPassagem(int idPassagem) {
		this.idPassagem = idPassagem;
	}
	public String getNome_Completo() {
		return Nome_Completo;
	}
	public void setNome_Completo(String nome_Completo) {
		Nome_Completo = nome_Completo;
	}
	public int getNumero_de_passagens() {
		return numero_de_passagens;
	}
	public void setNumero_de_passagens(int numero_de_passagens) {
		this.numero_de_passagens = numero_de_passagens;
	}
	public String getCPF() {
		return CPF;
	}
	public void setCPF(String CPF) {
		this.CPF = CPF;
	}
	public String getRG() {
		return RG;
	}
	public void setRG(String RG) {
		this.RG = RG;
	}
	public String getCartao_de_Credito() {
		return Cartao_de_Credito;
	}
	public void setCartao_de_Credito(String Cartao_de_Credito) {
		this.Cartao_de_Credito = Cartao_de_Credito;
	}
	public String getCVV() {
		return CVV;
	}
	public void setCVV(String CVV) {
		this.CVV = CVV;
	}
	public String getGenero_pass() {
		return Genero_pass;
	}
	public void setGenero_pass(String Genero_pass) {
		this.Genero_pass = Genero_pass;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getRua() {
		return Rua;
	}
	public void setRua(String rua) {
		Rua = rua;
	}
	public String getNumero() {
		return Numero;
	}
	public void setNumero(String Numero) {
		this.Numero = Numero;
	}
	public String getBairro() {
		return Bairro;
	}
	public void setBairro(String bairro) {
		Bairro = bairro;
	}
	public int getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(int id_cliente) {
		this.id_cliente = id_cliente;
	}
	public String getTelefone() {
		return Telefone;
	}
	public void setTelefone(String Telefone) {
		this.Telefone = Telefone;
	}
	public static void add(Passageiro f) {
		// TODO Auto-generated method stub
		
	}
}
