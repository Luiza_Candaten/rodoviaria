package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastrarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField Nome_Completo;
	private JTextField Genero_pass;
	private JTextField RG;
	private JTextField CPF;
	private JTextField Rua;
	private JTextField Numero;
	private JTextField Bairro;
	private JTextField Email;
	private JTextField Telefone;
	private JTextField Cartao_de_Credito;
	private JTextField CVV;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastrarPassageiro frame = new JFCadastrarPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastrarPassageiro() {
		setTitle("SisRodoviaria - Cadastrar Passageiro");
		getContentPane().setBackground(new Color(240, 240, 240));
		getContentPane().setLayout(null);
		setForeground(new Color(240, 240, 240));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 335);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Nome = new JLabel("Nome Completo:");
		Nome.setFont(new Font("Tahoma", Font.BOLD, 11));
		Nome.setBounds(10, 33, 109, 13);
		contentPane.add(Nome);
		
		JLabel Id_Generoquest = new JLabel("Genero do passageiro:");
		Id_Generoquest.setFont(new Font("Tahoma", Font.BOLD, 11));
		Id_Generoquest.setBounds(10, 56, 128, 13);
		contentPane.add(Id_Generoquest);
		
		Genero_pass = new JTextField();
		Genero_pass.setColumns(10);
		Genero_pass.setBounds(148, 56, 96, 13);
		contentPane.add(Genero_pass);
		
		Nome_Completo = new JTextField();
		Nome_Completo.setBounds(127, 33, 96, 13);
		contentPane.add(Nome_Completo);
		Nome_Completo.setColumns(10);
		
		JLabel RGq = new JLabel("RG:");
		RGq.setFont(new Font("Tahoma", Font.BOLD, 11));
		RGq.setBounds(10, 79, 45, 13);
		contentPane.add(RGq);
		
		JLabel CPFq = new JLabel("CPF:");
		CPFq.setFont(new Font("Tahoma", Font.BOLD, 11));
		CPFq.setBounds(10, 102, 45, 13);
		contentPane.add(CPFq);
		
		JLabel Ruaq = new JLabel("Rua:");
		Ruaq.setFont(new Font("Tahoma", Font.BOLD, 11));
		Ruaq.setBounds(10, 125, 45, 13);
		contentPane.add(Ruaq);
		
		JLabel Numeroq = new JLabel("Numero");
		Numeroq.setFont(new Font("Tahoma", Font.BOLD, 11));
		Numeroq.setBounds(10, 145, 45, 13);
		contentPane.add(Numeroq);
		
		JLabel Bairroq = new JLabel("Bairro:");
		Bairroq.setFont(new Font("Tahoma", Font.BOLD, 11));
		Bairroq.setBounds(10, 168, 45, 13);
		contentPane.add(Bairroq);
		
		JLabel Emailq = new JLabel("Email:");
		Emailq.setFont(new Font("Tahoma", Font.BOLD, 11));
		Emailq.setBounds(10, 191, 45, 13);
		contentPane.add(Emailq);
		
		JLabel Telefoneq = new JLabel("Telefone:");
		Telefoneq.setFont(new Font("Tahoma", Font.BOLD, 11));
		Telefoneq.setBounds(10, 214, 69, 13);
		contentPane.add(Telefoneq);
		
		RG = new JTextField();
		RG.setBounds(45, 79, 96, 13);
		contentPane.add(RG);
		RG.setColumns(10);
		
		CPF = new JTextField();
		CPF.setColumns(10);
		CPF.setBounds(45, 99, 96, 13);
		contentPane.add(CPF);
		
		Rua = new JTextField();
		Rua.setColumns(10);
		Rua.setBounds(45, 122, 96, 13);
		contentPane.add(Rua);
		
		Numero = new JTextField();
		Numero.setColumns(10);
		Numero.setBounds(68, 145, 96, 13);
		contentPane.add(Numero);
		
		Bairro = new JTextField();
		Bairro.setColumns(10);
		Bairro.setBounds(68, 168, 96, 13);
		contentPane.add(Bairro);
		
		Email = new JTextField();
		Email.setColumns(88);
		Email.setBounds(68, 188, 96, 13);
		contentPane.add(Email);
		
		Telefone = new JTextField();
		Telefone.setColumns(10);
		Telefone.setBounds(68, 211, 96, 13);
		contentPane.add(Telefone);
		
		JLabel cartao_de_creditoq = new JLabel("Cartao de Credito:");
		cartao_de_creditoq.setFont(new Font("Tahoma", Font.BOLD, 11));
		cartao_de_creditoq.setBounds(10, 237, 109, 13);
		contentPane.add(cartao_de_creditoq);
		
		JLabel CVVq = new JLabel("CVV:");
		CVVq.setFont(new Font("Tahoma", Font.BOLD, 11));
		CVVq.setBounds(218, 237, 45, 13);
		contentPane.add(CVVq);
		
		Cartao_de_Credito = new JTextField();
		Cartao_de_Credito.setColumns(10);
		Cartao_de_Credito.setBounds(117, 237, 91, 13);
		contentPane.add(Cartao_de_Credito);
		
		CVV = new JTextField();
		CVV.setColumns(10);
		CVV.setBounds(250, 237, 96, 13);
		contentPane.add(CVV);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Passageiro f = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				f.setNome_Completo(Nome_Completo.getText());
				f.setGenero_pass(Genero_pass.getText());
				f.setRG(RG.getText());
				f.setCPF(CPF.getText());
				f.setRua(Rua.getText());
				f.setNumero(Numero.getText());
				f.setBairro(Bairro.getText());
				f.setEmail(Email.getText());
				f.setTelefone(Telefone.getText());
				f.setCartao_de_Credito(Cartao_de_Credito.getText());
				f.setCVV(CVV.getText());
				
				dao.create(f);
			}
		}
);
		btnCadastrar.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCadastrar.setBounds(20, 267, 99, 21);
		contentPane.add(btnCadastrar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnCancelar.setBounds(151, 267, 85, 21);
		contentPane.add(btnCancelar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Nome_Completo.setText(null);
				Genero_pass.setText(null);
				CPF.setText(null);
				Rua.setText(null);
				RG.setText(null);
				Numero.setText(null);
				Genero_pass.setText(null);
				Email.setText(null);
				Rua.setText(null);
				Bairro.setText(null);
				Telefone.setText(null);		
			}
		});	
	}
}