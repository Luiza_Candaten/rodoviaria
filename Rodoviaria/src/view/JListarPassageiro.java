package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.bean.Passageiro;
import model.dao.PassageiroDAO;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class JListarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTable JTPassageiro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JListarPassageiro frame = new JListarPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JListarPassageiro() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readJTable();
			}
		});
		setTitle("Listar Passageiro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 691, 437);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblListarPassageiros = new JLabel("Listar Passageiro");
		lblListarPassageiros.setFont(new Font("Dialog", Font.BOLD, 18));
		lblListarPassageiros.setBounds(12, 12, 302, 28);
		contentPane.add(lblListarPassageiros);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 52, 639, 238);
		contentPane.add(scrollPane);
		
		JTPassageiro = new JTable();
		JTPassageiro.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
					"Genero", "Nome Completo", "CPF", "RG"
			}
		));
		scrollPane.setViewportView(JTPassageiro);
		
		JButton btnCadastrarPassageiro = new JButton("Cadastrar Passageiro");
		btnCadastrarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFCadastrarPassageiro cf = new JFCadastrarPassageiro();
				cf.setVisible(true);				
			}
		});
		btnCadastrarPassageiro.setBounds(22, 317, 155, 25);
		contentPane.add(btnCadastrarPassageiro);
		
		JButton btnAlterarPassageiro = new JButton("Alterar Passageiro");
		btnAlterarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				if(JTPassageiro.getSelectedRow()!= -1) {
					JFAtualizarPassageiro af = new JFAtualizarPassageiro((String)JTPassageiro.getValueAt(JTPassageiro.getSelectedRow(),0));
					af.setVisible(true);	
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um Passageiro!");
				}
				readJTable();				
			}
		});
		btnAlterarPassageiro.setBounds(206, 317, 140, 25);
		contentPane.add(btnAlterarPassageiro);
		
		JButton btnExcluirPassageiro = new JButton("Excluir Passageiro");
		btnExcluirPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(JTPassageiro.getSelectedRow()!= -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o Passageiro selecionado?","Exclus�o", JOptionPane.YES_NO_OPTION);
					if(opcao == 0) {
						PassageiroDAO dao = new PassageiroDAO();
						Passageiro f = new Passageiro();
						f.setId_cliente((int)JTPassageiro.getValueAt(JTPassageiro.getSelectedRow(),0));
						dao.delete(f);
					}
				}else {
					JOptionPane.showMessageDialog(null,"Selecione um Passageiro!");
				}
				readJTable();
			}
		});
		btnExcluirPassageiro.setBounds(380, 317, 140, 25);
		contentPane.add(btnExcluirPassageiro);
		
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTPassageiro.getModel();
		modelo.setNumRows(0);
		PassageiroDAO fdao = new PassageiroDAO();
		for(Passageiro f : fdao.read()) {
			modelo.addRow(new Object[] {
					f.getId_cliente(),
					f.getNome_Completo(),
					f.getGenero_pass(),
					f.getRG(),
					f.getCPF(),
					f.getRua(),
					f.getNumero(),
					f.getBairro(),
					f.getEmail(),
					f.getTelefone(),
					f.getCartao_de_Credito(),
					f.getCVV()
			});
		}
	}
	
}