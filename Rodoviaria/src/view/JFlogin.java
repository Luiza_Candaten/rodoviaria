package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class JFlogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtusuario;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFlogin frame = new JFlogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFlogin() {
		setTitle("SisRodoviaria-Tela de Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("SISRODOVIARIA - BEM VINDO!");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setBounds(11, 10, 300, 32);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Informe suas credenciais de acesso");
		lblNewLabel_1.setFont(new Font("Dialog", Font.BOLD, 12));
		lblNewLabel_1.setBounds(21, 52, 327, 32);
		contentPane.add(lblNewLabel_1);
		
		JLabel txtUsu�rio = new JLabel("Usuario:");
		txtUsu�rio.setFont(new Font("Dialog", Font.BOLD, 11));
		txtUsu�rio.setBounds(20, 86, 58, 14);
		contentPane.add(txtUsu�rio);
		
		txtusuario = new JTextField();
		txtusuario.setBounds(71, 85, 159, 15);
		contentPane.add(txtusuario);
		txtusuario.setColumns(10);
		
		JLabel IblSenha = new JLabel("Senha:");
		IblSenha.setFont(new Font("Dialog", Font.BOLD, 11));
		IblSenha.setBounds(21, 110, 45, 13);
		contentPane.add(IblSenha);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(71, 110, 159, 14);
		contentPane.add(txtSenha);
		
		JButton txtAcessar = new JButton("Acessar");
		txtAcessar.setBounds(27, 153, 85, 21);
		contentPane.add(txtAcessar);
		
		JButton txtCancelar = new JButton("Cancelar");
		txtCancelar.setBounds(172, 153, 85, 21);
		contentPane.add(txtCancelar);
		
		JButton txtCadastrar = new JButton("Cadastrar-se");
		txtCadastrar.setBounds(27, 197, 101, 21);
		contentPane.add(txtCadastrar);
		
		JButton txtRecuperarsenha = new JButton("Recuperar senha");
		txtRecuperarsenha.setBounds(172, 197, 111, 21);
		contentPane.add(txtRecuperarsenha);
	}
}
