-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 21-Set-2021 às 21:02
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `rodoviaria`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `id_funcionario` int(11) NOT NULL,
  `funcionario_nome` varchar(30) NOT NULL,
  `funcionario_cpf` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`id_funcionario`, `funcionario_nome`, `funcionario_cpf`) VALUES
(1, 'Tereza Liz Drumond', 349142),
(2, 'Evelyn Gabriela Nogueira', 914154),
(3, 'Ian Luís Renan Mendes', 8489),
(4, 'Anthony Yuri Julio Oliveira', 183038),
(5, 'Bruno Jorge Rezende', 844314);

-- --------------------------------------------------------

--
-- Estrutura da tabela `linha_horarios`
--

CREATE TABLE `linha_horarios` (
  `destino` varchar(50) NOT NULL,
  `origem` varchar(50) NOT NULL,
  `modalidade` varchar(30) NOT NULL,
  `data_hora` datetime NOT NULL,
  `dia_da_semana` varchar(50) NOT NULL,
  `CodRodoviaria` int(11) NOT NULL,
  `valor` int(11) NOT NULL,
  `Seguro` tinyint(4) NOT NULL,
  `poltrona` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `linha_horarios`
--

INSERT INTO `linha_horarios` (`destino`, `origem`, `modalidade`, `data_hora`, `dia_da_semana`, `CodRodoviaria`, `valor`, `Seguro`, `poltrona`) VALUES
('Cruz Alta', 'Erval Seco', 'Comum', '0000-00-00 00:00:00', 'quinta', 1, 46, 0, 5),
('Erval Seco', 'Cruz Alta', 'Semidireto', '0000-00-00 00:00:00', 'terça', 5, 24, 0, 14),
('Canela', 'Pelotas', 'Comum', '0000-00-00 00:00:00', 'quarta', 1, 50, 0, 12),
('Pelotas', 'Erval Seco', 'Direto', '0000-00-00 00:00:00', 'domingo', 5, 59, 0, 36),
('Alegrete', 'Canela', 'Direto', '2003-06-15 00:00:00', 'sabado', 2, 87, 0, 23),
('Panambi', 'Alegrete', 'Comum', '2028-02-10 10:00:00', 'segunda', 3, 65, 0, 25),
('Canela', 'Panambi', 'Comum', '0000-00-00 00:00:00', 'sexta', 2, 42, 0, 18),
('Gramado', 'Ijuí', 'Semidireto', '0000-00-00 00:00:00', 'domingo', 4, 33, 0, 9),
('Ijuí', 'Capão da Canoa', 'Comum', '2030-02-09 20:00:00', 'sexta', 3, 47, 0, 28),
('Panambi', 'Gramado', 'Semidireto', '2029-01-20 20:00:00', 'sexta', 4, 70, 0, 25);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagamento`
--

CREATE TABLE `pagamento` (
  `cartao_de_credito` int(11) DEFAULT NULL,
  `CVV` int(11) DEFAULT NULL,
  `preco_total` int(11) NOT NULL,
  `data_validade` date NOT NULL,
  `id_cliente` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `pagamento`
--

INSERT INTO `pagamento` (`cartao_de_credito`, `CVV`, `preco_total`, `data_validade`, `id_cliente`) VALUES
(6062, 333, 50, '0000-00-00', '2'),
(5037, 589, 59, '0000-00-00', '1'),
(3024, 441, 46, '0000-00-00', '3'),
(5305, 551, 24, '0000-00-00', '4'),
(3029, 999, 65, '0000-00-00', '5'),
(NULL, NULL, 0, '2021-02-01', NULL),
(NULL, NULL, 0, '2021-02-02', NULL),
(NULL, NULL, 0, '2021-02-03', NULL),
(NULL, NULL, 0, '2021-02-04', NULL),
(NULL, NULL, 0, '2021-02-05', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `passageiro`
--

CREATE TABLE `passageiro` (
  `id_cliente` int(11) NOT NULL,
  `nome_cliente` varchar(60) DEFAULT NULL,
  `numero_de_passagens` int(11) NOT NULL,
  `cpf` int(11) NOT NULL,
  `cartao_de_credito` int(11) NOT NULL,
  `CVV` int(11) NOT NULL,
  `email` varchar(20) NOT NULL,
  `senha` char(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `passageiro`
--

INSERT INTO `passageiro` (`id_cliente`, `nome_cliente`, `numero_de_passagens`, `cpf`, `cartao_de_credito`, `CVV`, `email`, `senha`) VALUES
(1, 'Emanuel Henry Bernardo Santos', 2, 559340, 5344, 788, 'emanuelhenrybernardo', '6PzBJh'),
(2, 'Vicente Tomás Geraldo Lopes', 1, 746647, 5508, 548, 'vicentetomasgeraldol', 'k3fzBU'),
(3, 'Antonio Vicente Geraldo Novaes', 2, 397566, 5202, 415, 'antoniovicentegerald', 'CrLtYd'),
(4, 'Tereza Alessandra Lívia Cardoso', 1, 457778, 5353, 272, 'terezaalessandralivi', 'DrTnN0'),
(5, 'Ayla Cristiane da Mata', 1, 108669, 2149, 588, 'aylacristianedamata.', 'FmKOKc'),
(6, 'Simone Laura Caldeira', 1, 989333, 2014, 604, 'terezaalessandralivi', 'WskXDY'),
(7, 'Vicente Murilo Novaes', 3, 691550, 6062, 385, 'vicentemurilonovaes-', 'Gw0ePR'),
(8, 'Joana Cristiane', 1, 949924, 2014, 143, 'jjoanacristianefogac', 'kmxBHd'),
(9, 'Anderson Carlos Eduardo Teixeira', 2, 121340, 5161, 827, 'andersoncarloseduard', 'QECZdf'),
(10, 'Tereza Liz Drumond', 1, 802478, 6062, 542, 'terezalizdrumond__te', 'naZ10K');

-- --------------------------------------------------------

--
-- Estrutura da tabela `passagem`
--

CREATE TABLE `passagem` (
  `id_passagem` int(11) NOT NULL,
  `empresa` varchar(15) NOT NULL,
  `box` int(11) NOT NULL,
  `numero_poltrona` int(11) NOT NULL,
  `valor_passagem` int(11) NOT NULL,
  `seguro` tinyint(4) NOT NULL,
  `funcionario_nome` varchar(30) NOT NULL,
  `nome_cliente` varchar(60) NOT NULL,
  `data_emissao` datetime NOT NULL,
  `endereco` varchar(50) NOT NULL,
  `cnpj` varchar(14) NOT NULL,
  `modalidade` varchar(30) NOT NULL,
  `destino` varchar(50) NOT NULL,
  `origem` varchar(50) NOT NULL,
  `data_hora` datetime NOT NULL,
  `voucher` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `passagem`
--

INSERT INTO `passagem` (`id_passagem`, `empresa`, `box`, `numero_poltrona`, `valor_passagem`, `seguro`, `funcionario_nome`, `nome_cliente`, `data_emissao`, `endereco`, `cnpj`, `modalidade`, `destino`, `origem`, `data_hora`, `voucher`) VALUES
(1, 'Mariana e José ', 1, 16, 45, 5, 'Tereza Liz Drumond', 'Emanuel Henry Bernardo Santos', '2021-02-04 00:00:00', 'Travessa Andaraí, 243, Aparecida', '20.273.516/000', 'Comum', 'Cruz Alta', 'Soledade', '2021-03-06 15:00:00', 8968),
(2, 'Aurora e Maitê ', 2, 14, 55, 15, 'Evelyn Gabriela Nogueira', 'Emanuel Henry Bernardo Santos', '2021-02-24 00:00:00', 'Travessa Andaraí, 243, Aparecida', '40.915.628/000', 'Comum', 'Pelotas', 'Não-Me-Toque', '2021-02-16 17:20:00', 3240),
(3, 'Mariana e José ', 1, 36, 65, 35, 'Tereza Liz Drumond', 'Vicente Tomás Geraldo Lopes', '2021-02-14 00:00:00', 'Rua Engenheiro Otávio Lago, 952', '17.212.979/000', 'Semidireto', 'Panambi', 'Cruz Alta', '2021-01-18 07:30:00', 6466),
(4, 'Eduardo e Samue', 2, 23, 150, 25, 'Ian Luís Renan Mendes', 'Antonio Vicente Geraldo Novaes', '2021-02-10 00:00:00', 'Rua Doze, 926', '12.558.867/000', 'Semidireto', 'Capão da Canoa', 'Passo Fundo', '2021-03-16 17:25:00', 8279),
(5, 'Camila e Mirell', 3, 24, 75, 35, 'Anthony Yuri Julio Oliveira', 'Tereza Alessandra Lívia Cardoso', '2021-03-14 00:00:00', 'Rua Doze, 926', '78.527.579/000', 'Comum', 'Ijuí', 'Carazinho', '2021-02-16 12:40:00', 1234),
(6, 'Eduardo e Samue', 3, 6, 23, 25, 'Bruno Jorge Rezende', 'Ayla Cristiane da Mata', '2021-01-24 00:00:00', 'Rua Coronel Aparício Borges, 626', '10.758.232/000', 'Comum', 'Alegrete', 'Ijuí', '2021-02-16 17:20:00', 3421),
(7, 'Rita e Tereza L', 4, 18, 36, 13, 'Bruno Jorge Rezende', 'Simone Laura Caldeira', '2021-02-11 00:00:00', 'Avenida General Flores da Cunha 545', '00.348.718/000', 'Direto', 'Erval Seco', 'Santo Ângelo', '2021-01-27 22:40:00', 7956),
(8, 'Camila e Mirell', 1, 30, 26, 80, 'Anthony Yuri Julio Oliveira', 'Tereza Liz Drumond', '2021-01-30 00:00:00', 'Rua Engenheiro Otávio Lago, 952', '37.964.651/000', 'Direto', 'Novo Hamburgo', 'Cerro Largo', '2021-01-31 11:45:00', 1213),
(9, 'Rita e Tereza L', 1, 1, 16, 11, 'Bruno Jorge Rezende', 'Vicente Tomás Geraldo Lopes', '2021-08-04 00:00:00', 'Avenida General Flores da Cunha 545', '63.398.216/000', 'Semidireto', 'Erval Seco', 'Sananduva', '2021-07-16 09:50:00', 9924),
(10, 'Rita e Tereza L', 3, 35, 15, 31, 'Evelyn Gabriela Nogueira', 'Ayla Cristiane da Mata', '2021-02-28 00:00:00', 'Rua Coronel Aparício Borges, 626', '34.385.518/000', 'Direto', 'Novo Hamburgo', 'Vacaria', '2021-01-18 19:20:00', 5670),
(11, 'Camila e Mirell', 2, 11, 56, 15, 'Anthony Yuri Julio Oliveira', 'Simone Laura Caldeira', '2021-12-02 00:00:00', 'Rua Doze, 926', '07.248.926/000', 'Comum', 'Alegrete', 'Guaporé', '2021-01-23 23:00:00', 7169),
(12, 'Aurora e Maitê ', 4, 29, 125, 35, 'Tereza Liz Drumond', 'Joana Cristiane', '2021-02-17 00:00:00', 'Rua Engenheiro Otávio Lago, 952', '29.214.556/000', 'Semidireto', 'Ijuí', 'Santa Maria', '2021-02-02 20:20:00', 983),
(13, 'Eduardo e Samue', 1, 5, 56, 56, 'Ian Luís Renan Mendes', 'Tereza Liz Drumond', '2021-02-03 00:00:00', 'Travessa Andaraí, 243, Aparecida', '49.227.018/000', 'Semidireto', 'Capão da Canoa', 'Restinga Seca', '2021-08-21 21:45:00', 5680),
(14, 'Aurora e Maitê ', 4, 20, 65, 15, 'Evelyn Gabriela Nogueira', 'Vicente Murilo Novaes', '2021-04-04 00:00:00', 'Rua Engenheiro Otávio Lago, 952', '10.935.906/000', 'Comum', 'Canela', 'Santa Cruz do Sul', '2021-11-01 17:25:00', 1125),
(15, 'Mariana e José ', 1, 9, 175, 5, 'Tereza Liz Drumond', 'Joana Cristiane', '2021-03-14 00:00:00', 'Travessa Andaraí, 243, Aparecida', '27.797.887/000', 'Comum', 'Canela', 'Porto Alegre', '2021-02-01 14:00:00', 1102);

-- --------------------------------------------------------

--
-- Estrutura da tabela `rodoviaria`
--

CREATE TABLE `rodoviaria` (
  `CodRodoviaria` int(11) NOT NULL,
  `nome` varchar(30) DEFAULT NULL,
  `endereco` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `rodoviaria`
--

INSERT INTO `rodoviaria` (`CodRodoviaria`, `nome`, `endereco`) VALUES
(1, 'Mariana e José ME', 'Travessa Andaraí, 243, Aparecida'),
(2, 'Aurora e Maitê Ltda', 'Rua Engenheiro Otávio Lago, 952'),
(3, 'Eduardo e Samuel Ltda', 'Rua Doze, 926'),
(4, 'Camila e Mirella Ltda', 'Avenida General Flores da Cunha 545'),
(5, 'Rita e Tereza Ltda', 'Rua Coronel Aparício Borges, 626');

-- --------------------------------------------------------

--
-- Estrutura da tabela `rodoviaria_cidade`
--

CREATE TABLE `rodoviaria_cidade` (
  `nome` varchar(50) DEFAULT NULL,
  `CEP` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `rodoviaria_cidade`
--

INSERT INTO `rodoviaria_cidade` (`nome`, `CEP`) VALUES
('Panambi', '98280-970'),
('Palmeira das Missões', '98300-977'),
('Novo Hamburgo', '93542-170'),
('Pelotas', '6020-220'),
('Frederico Westphalen', '98400-000'),
('Erval Seco', '98390-000'),
('Erechim', '99704-220'),
('Alegrete', '97546-070'),
('Ijuí', '98700-973'),
(' Rio Grande', '96200-370'),
('Garibaldi', '95720-970'),
('Flores da Cunha', '95270-977'),
('Gramado', '95670-971'),
('Estrela', '95880-970'),
('Passo Fundo', '99070-000'),
('Imbé', '95625-000'),
('Gentil', '99160-970'),
('Canela', '95680-971'),
('Barra Funda', '99585-000'),
('Carlos Barbosa', '95185-000'),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL),
(NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `voucher`
--

CREATE TABLE `voucher` (
  `voucher` int(15) NOT NULL,
  `id_cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `voucher`
--

INSERT INTO `voucher` (`voucher`, `id_cliente`) VALUES
(8968, 1),
(1213, 2),
(9924, 3),
(7131, 4),
(4695, 5);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`id_funcionario`);

--
-- Índices para tabela `linha_horarios`
--
ALTER TABLE `linha_horarios`
  ADD KEY `CodRodoviaria` (`CodRodoviaria`);

--
-- Índices para tabela `passageiro`
--
ALTER TABLE `passageiro`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Índices para tabela `passagem`
--
ALTER TABLE `passagem`
  ADD PRIMARY KEY (`id_passagem`);

--
-- Índices para tabela `rodoviaria`
--
ALTER TABLE `rodoviaria`
  ADD PRIMARY KEY (`CodRodoviaria`);

--
-- Índices para tabela `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`voucher`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `id_funcionario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `passageiro`
--
ALTER TABLE `passageiro`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `passagem`
--
ALTER TABLE `passagem`
  MODIFY `id_passagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `rodoviaria`
--
ALTER TABLE `rodoviaria`
  MODIFY `CodRodoviaria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `linha_horarios`
--
ALTER TABLE `linha_horarios`
  ADD CONSTRAINT `linha_horarios_ibfk_1` FOREIGN KEY (`CodRodoviaria`) REFERENCES `rodoviaria` (`CodRodoviaria`);

--
-- Limitadores para a tabela `voucher`
--
ALTER TABLE `voucher`
  ADD CONSTRAINT `voucher_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `passageiro` (`id_cliente`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
